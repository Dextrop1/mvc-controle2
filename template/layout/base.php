<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash()) ?>
<header style="background-color: black; color: white;" id="masthead">
    <nav>
        <ul style="display: flex;justify-content: space-around;padding: 2rem;     margin: 0;
;
">
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path(''); ?>">Home</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('register'); ?>">register de user</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('listing'); ?>">listing de user</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('register-salle'); ?>">register de salle</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('listing-salle'); ?>">listing de salle</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('register-creneau'); ?>">register de creneau</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('listing-creneau'); ?>">listing de creneau</a></li>
        </ul>
    </nav>
</header>

<div class="container">
    <?= $content; ?>
</div>

<footer id="colophon">
    <div style="" class="wrap">
        <p>MVC 6 - Framework Pédagogique.</p>
    </div>
</footer>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
