<form action="" method="post" novalidate>
    <?php echo $form->label('id_salle'); ?>
    <?php echo $form->input('id_salle', $creneau->id_salle ?? ''); ?>
    <?php echo $form->error('id_salle'); ?>

    <?php echo $form->label('start_at'); ?>
    <?php echo $form->input('start_at', $creneau->start_at ?? ''); ?>
    <?php echo $form->error('start_at'); ?>

    <?php echo $form->label('nbrehours'); ?>
    <?php echo $form->input('nbrehours', $creneau->nbrehours ?? ''); ?>
    <?php echo $form->error('nbrehours'); ?>

    <?php echo $form->submit('submitted', $textButton); ?>
</form>
<style>
    .container {
        width: 500px;
        margin: 0 auto;
    }
    span.error {
        color: red;
    }
</style>