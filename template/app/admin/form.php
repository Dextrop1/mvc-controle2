<form action="" method="post" novalidate>

    <?php echo $form->label('email'); ?>
    <?php echo $form->input('email', $user->email ?? ''); ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->label('nom'); ?>
    <?php echo $form->input('nom', $user->nom ?? ''); ?>
    <?php echo $form->error('nom'); ?>


    <?php echo $form->submit('submitted', $textButton); ?>
</form>
<style>
    .container {
        width: 500px;
        margin: 0 auto;
    }
    span.error {
        color: red;
    }
</style>