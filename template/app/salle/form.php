<form action="" method="post" novalidate>

    <?php echo $form->label('title'); ?>
    <?php echo $form->input('title', $salle->title ?? ''); ?>
    <?php echo $form->error('title'); ?>

    <?php echo $form->label('maxuser'); ?>
    <?php echo $form->input('maxuser', $salle->maxuser ?? ''); ?>
    <?php echo $form->error('maxuser'); ?>


    <?php echo $form->submit('submitted', $textButton); ?>
</form>
<style>
    .container {
        width: 500px;
        margin: 0 auto;
    }
    span.error {
        color: red;
    }
</style>