<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class AdminModel extends AbstractModel
{
    protected static $table = 'user';

    protected $id;
    protected $nom;
    protected $email;
    protected $super;

    public function getSuper()
    {
        return mb_strtoupper($this->nom);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (nom, email) VALUES (?, ?)",
            array($post['nom'], $post['email'])
        );
    }
}