<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel
{
    protected static $table = 'creneau';

    protected $id;
    protected $id_salle;
    protected $start_at;
    protected $nbrehours;

    protected $super;

    public function getSuper()
    {
        return mb_strtoupper($this->id_salle);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getId_salle()
    {
        return $this->id_salle;
    }

    public function getStart_at()
    {
        return $this->start_at;
    }
    public function getNbrehours()
    {
        return $this->nbrehours;
    }
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_salle, start_at,nbrehours) VALUES (?, ?, ?)",
            array($post['id_salle'], $post['start_at'], $post['nbrehours'])
        );
    }
}