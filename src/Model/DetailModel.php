<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class DetailModelModel extends AbstractModel
{
    protected static $table = 'creneau_user';


    protected $id;
    protected $title;
    protected $super;

    public function getSuper()
    {
        return mb_strtoupper($this->title);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }



}