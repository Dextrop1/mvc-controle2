<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\Config;

class CreneauController extends AbstractController
{
    private $statusList;
    private $v;

    public function __construct()
    {
        $this->statusList = (new Config())->get('listStatus');
        $this->v = new Validation();
    }

    public function listingCreneau()
    {
        $this->render('app.creneau.listing-creneau', array(
            'creneaus' => CreneauModel::all()
        ), 'admin');
    }

    public function registerCreneau()
    {
        $errors = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if ($this->v->isValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Créneau enregistré avec succès !');
                $this->redirect('listing-creneau');
            }
        }
        $form = new Form($errors);
        $this->render('app.creneau.register-creneau', array(
            'form' => $form,
            'statusList' => $this->statusList
        ), 'admin');
    }

    private function validate($v, $post)
    {
        $errors = [];
        $errors['id_salle'] = isset($post['id_salle']) ? $v->textValid($post['id_salle'], 'id_salle', 2, 100) : '';
        $errors['maxuser'] = $v->textValid($post['start_at'], 'start_at',1, 2);
        $errors['nbrehours'] = !ctype_digit($post['nbrehours']) ? 'nbrehours must be a number' : '';

        return $errors;
    }
}