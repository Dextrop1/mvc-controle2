<?php

namespace App\Controller;

use App\Model\AdminModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\Config;

class SecurityController extends AbstractController
{
    private $statusList;
    private $v;

    public function __construct()
    {
        $this->statusList = (new Config())->get('listStatus');
        $this->v = new Validation();
    }

    public function listing() {
        $this->render('app.admin.listing', array(
            'users' => AdminModel::all()
        ), 'admin');
    }



    public function register() {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                var_dump($post);
               AdminModel::insert($post);
                $this->addFlash('success', 'Message good envoy!');
                $this->redirect('listing');
            }
        }
        $form = new Form($errors);
        $this->render('app.admin.register', array(
            'form' => $form,
            'statusList' => $this->statusList
        ),'admin');
    }




    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',2, 100);
        $errors['email'] = $v->textValid($post['email'], 'email',5, 50);
        return $errors;
    }


}
