<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\Config;

class DetailController extends AbstractController
{
    private $statusList;
    private $v;



    public function listingCreneauDetail()
    {
        $this->render('app.detail.detail', array(
            'salles' => SalleModel::all()
        ), 'admin');
    }




}