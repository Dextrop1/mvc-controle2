<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\Config;

class SalleController extends AbstractController
{
    private $statusList;
    private $v;

    public function __construct()
    {
        $this->statusList = (new Config())->get('listStatus');
        $this->v = new Validation();
    }

    public function listingSalle() {
        $this->render('app.salle.listing-salle', array(
            'salles' => SalleModel::all()
        ), 'admin');
    }

    public function registerSalle() {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v,$post);
            if($this->v->isValid($errors)) {
                var_dump($post);
                SalleModel::insert($post);
                $this->addFlash('success', 'Salle enregistrée avec succès !');
                $this->redirect('listing-salle');
            }
        }
        $form = new Form($errors);
        $this->render('app.salle.register-salle', array(
            'form' => $form,
            'statusList' => $this->statusList
        ),'admin');
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',2, 100);
        $errors['maxuser'] = $v->textValid($post['maxuser'], 'maxuser',5, 50);
        return $errors;
    }
}